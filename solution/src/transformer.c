#include "transformer.h"

struct image rotate_image(struct image image){
    struct image rotated_image = create_image(image.height, image.width);

    for(uint64_t i = 0; i < image.height; i++){
        for(uint64_t j = 0; j < image.width; j++){
            rotated_image.data[rotated_image.width * j + rotated_image.width - i - 1] = image.data[image.width * i + j];
        }
    }
    return rotated_image;
}
