#include <stdio.h>

#include "bmp_header.h"
#include "image.h"
#include "transformer.h"
#include "utils.h"

void usage(void){
    print_error("Usage: ./" "image_transformer" " input_file_name output_file_name");
}


int main( int argc, char** argv ) {


    if (argc < 3) { print_error("Not enough arguments"); usage(); return 0;}

    FILE* input_file = fopen(argv[1], "rb");
    if(input_file == NULL) {
        printf("Cannot open input file\n");
        return -1;
    }

    FILE* output_file = fopen(argv[2], "wb");
    if(output_file == NULL) {
        printf("Cannot open output file\n");
        return -1;
    }

    struct image image = {0};
    const enum bmp_read_status_code read_code = read_bmp_image(&image, input_file); // read file
    if(read_code) return read_code;

    struct image rotated_image = rotate_image(image);
    const enum bmp_write_status_code write_code = write_bmp_image(&rotated_image, output_file);
    if(write_code) return write_code;
    free_image(&image);
    free_image(&rotated_image);

    return 0;
}
