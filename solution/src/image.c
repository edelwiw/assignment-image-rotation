#include "image.h"

struct image create_image(int32_t width, int32_t height) {
    struct image image;
    image.width = width;
    image.height = height;
    image.data = malloc(sizeof(struct pixel) * height * width);
    return image;
}

void free_image(struct image* image){
    if(image->data) free(image->data);
}
