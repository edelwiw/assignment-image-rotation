#include "bmp_header.h"

static enum bmp_read_status_code check_bmp_header(const struct bmp_header* header){
    if(header -> bfType != BMP_TYPE) return BAD_BMP_SIGNATURE; // Not BMP file
    if(header -> biBitCount != 24) return UNSUPPORTED_BPP; // Not 24 bit image
    return SUCCESS_READ;
}

enum bmp_read_status_code read_bmp_header(struct bmp_header* bmpHeader, FILE* file){
    size_t read_bytes = fread(bmpHeader, sizeof (struct bmp_header), 1, file);
    if(!read_bytes)  return FILE_READING_ERROR;
    return check_bmp_header(bmpHeader);
}

static uint64_t calculate_padding_bytes(uint64_t width){
    uint64_t padding = (4 - width * sizeof (struct pixel) % 4) % 4;
//    printf("Padding %lld \n", padding);
    return padding;
}

static enum bmp_read_status_code read_pixels(struct image* image, struct bmp_header* bmpHeader, FILE* input_image_file){
    uint64_t padding = calculate_padding_bytes(bmpHeader->biWidth);
    for (uint32_t i = 0; i < bmpHeader->biHeight; i++) {
        if (fread( image->data + (i * bmpHeader->biWidth), sizeof(struct pixel), bmpHeader->biWidth, input_image_file) < bmpHeader->biWidth ||
            fseek(input_image_file, (int)padding, SEEK_CUR) != 0) return FILE_READING_ERROR;
        }
    return SUCCESS_READ;
}

enum bmp_read_status_code read_bmp_image(struct image* image, FILE* input_image_file){
    // open source file
    if (!input_image_file) return FILE_READING_ERROR;

    // read headers
    struct bmp_header bmpHeader;
    enum bmp_read_status_code read_status_code = read_bmp_header(&bmpHeader, input_image_file);
    if(read_status_code) {fclose(input_image_file); return read_status_code;}

    //  read image
    *image = create_image(bmpHeader.biWidth, bmpHeader.biHeight);
    if(image -> data == NULL) return IMAGE_CREATING_ERROR;
    read_status_code = read_pixels(image, &bmpHeader, input_image_file);
    if(read_status_code) {fclose(input_image_file); return read_status_code;}

    fclose(input_image_file);

    return SUCCESS_READ;
}

static struct bmp_header generate_bmp_header(const struct image* image) {
    const uint32_t header_size = sizeof(struct bmp_header);
    const uint32_t img_size = sizeof(struct pixel) * image->height * (image->width + calculate_padding_bytes(image->width));
    const uint32_t file_size = header_size + img_size;

    struct bmp_header header = {
            .bfType = BMP_TYPE,
            .bfSize = file_size,
            .bfReserved1 = 0,
            .bfReserved2 = 0,
            .bfOffBits = header_size,

            .biSize = BMP_INFO_SIZE,
            .biWidth = image->width,
            .biHeight = image->height,
            .biPlanes = 1,
            .biBitCount = BMP_BIT_COUNT,
            .biCompression = 0,
            .biSizeImage = 0,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0
    };

    return header;
}

enum bmp_write_status_code write_bmp_image(struct image* image, FILE* output_image_file){
    struct bmp_header header = generate_bmp_header(image);

    if (fwrite(&header, sizeof(struct bmp_header), 1, output_image_file) != 1) {
        return HEADER_WRITING_ERROR;
    }
    char o[] = "DUMMASAN";
    for (uint32_t i = 0; i < image->height; i++) {
        if (fwrite(&image->data[i * image->width], sizeof(struct pixel), image->width, output_image_file) != image->width
            || !fwrite(&o, 1, calculate_padding_bytes(image->width), output_image_file)) {
            return FILE_WRITING_ERROR;
        }
    }
    return SUCCESS_WRITE;
}
