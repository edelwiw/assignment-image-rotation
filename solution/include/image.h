#include <stdint.h>
#include <stdlib.h>

#ifndef IMAGE_TRANSFORMER_IMAGE_H
#define IMAGE_TRANSFORMER_IMAGE_H

#pragma pack(push, 1)
struct pixel{ // for 24 bit encode
    uint8_t B, G, R;
};
#pragma pack(pop)

struct image{
    int32_t width, height;
    struct pixel* data;
};

void free_image(struct image* image);
struct image create_image(int32_t width, int32_t height);

#endif //IMAGE_TRANSFORMER_IMAGE_H
