#include <stdio.h>
#include <stdlib.h>

#include "image.h"

#ifndef IMAGE_TRANSFORMER_BMP_HEADER_H
#define IMAGE_TRANSFORMER_BMP_HEADER_H

#define BMP_TYPE 0x4D42
#define BMP_BIT_COUNT 24 // bits per pixel
#define BMP_INFO_SIZE 40

#pragma pack(push, 1)
struct bmp_header {
    uint16_t bfType; // Magic identifier: 0x4d42
    uint32_t bfSize; // File size in bytes
    uint16_t bfReserved1; // Not used
    uint16_t bfReserved2; // Not used
    uint32_t bfOffBits; // Offset to image data in bytes from beginning of this structure

    uint32_t biSize; // DIB Header size in bytes
    int32_t  biWidth; // Width of the image
    int32_t  biHeight; // Height of image and order in 2-dim arrays (sign)
    uint16_t biPlanes; // Number of color planes
    uint16_t biBitCount; // Bits per pixel
    uint32_t biCompression; // Compression type
    uint32_t biSizeImage; // Image size in bytes
    int32_t  biXPelsPerMeter; // Pixels per meter
    int32_t  biYPelsPerMeter; // Pixels per meter
    uint32_t biClrUsed; // Number of colors
    uint32_t biClrImportant; // Important colors
};

#pragma pack(pop)

enum bmp_read_status_code{
    SUCCESS_READ,
    BAD_BMP_SIGNATURE,
    UNSUPPORTED_BPP, // bits per pixel
    FILE_READING_ERROR,
    IMAGE_CREATING_ERROR,
};

enum bmp_write_status_code{
    SUCCESS_WRITE,
    HEADER_WRITING_ERROR,
    FILE_WRITING_ERROR,
};

enum bmp_read_status_code read_bmp_image(struct image* image, FILE* input_image_file);
enum bmp_write_status_code write_bmp_image(struct image* image, FILE* output_image_file);

#endif //IMAGE_TRANSFORMER_BMP_HEADER_H
